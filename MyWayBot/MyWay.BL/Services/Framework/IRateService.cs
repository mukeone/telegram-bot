﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyWay.BL.BusinessEntities;
using MyWay.BL.BusinessEntities.Elevations;
using MyWayBot.Storage.Commands.Rate;

namespace MyWay.BL.Services.Framework
{
    public interface IRateService
    {
	    Task<int> AddRate(long chatId, int attitudeId, int rate, string comment);
	    Task<List<Elevation>> LoadChatElevations(long chatId);
	    Task<ElevationsHistory> LoadChatElevationsHistory(long chatId);
	    Task<Attitude> LoadNextAttitudeByChat(long chatId);
    }
}
