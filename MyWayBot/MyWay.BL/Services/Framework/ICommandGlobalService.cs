﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyWay.BL.Bot.Commands;

namespace MyWay.BL.Services.Framework
{
    public interface ICommandGlobalService
    {
	    Task<int> SaveAction(ICommand command, long chatId);
    }
}
