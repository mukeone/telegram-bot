﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MyWay.BL.Bot.Commands;
using MyWay.BL.BusinessEntities;
using MyWay.BL.Services.Framework;
using MyWayBot.Storage.EntityFramework;
using MyWayBot.Storage.Solution;

namespace MyWay.BL.Services.Implementations
{
    public class GlobalCommandMetaSaver : ICommandGlobalService
    {
	    private MyWayContext _storage;

	    public GlobalCommandMetaSaver(MyWayContext storage)
	    {
		    _storage = storage;
	    }

	    public async Task<int> SaveAction(ICommand command, long chatId)
	    {
		    var action = ActionBuilder.Builder.SetChat(chatId)
							.Command(command.Name).Build();
		    var actionLogId = await WriteRecord(action);
		    return actionLogId;
	    }

	    private async Task<int> WriteRecord(CommandsController action)
	    {
		    var entry = await _storage.Controller.AddAsync(action);
		    await _storage.SaveChangesAsync();
		    return entry.Entity.Id;
	    }
    }
}
