﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyWay.BL.BusinessEntities;
using MyWay.BL.Exceptions;
using MyWay.BL.Services.Framework;
using MyWayBot.Storage.Commands.Rate;
using MyWayBot.Storage.EntityFramework;

namespace MyWay.BL.Services.Implementations
{
    public class RateService : IRateService
    {
	    private MyWayContext _storage;

	    public RateService(MyWayContext storage)
	    {
		    _storage = storage;
	    }
		
	    public async Task<int> AddRate(long chatId, int attitudeId, int rate, string comment)
	    {
			var attitude = await FindAttitude(attitudeId);
			var elevation = ElevationBuilder.Builder().ChatId(chatId)
				.Dependencies(attitudeId).Rate(rate, comment).Build();
			var elevationId = await SaveElevation(elevation);
		    var workflowId = await SaveWorkflow(attitude.Number, chatId);
		    return elevationId;
	    }

	    private async Task<Attitude> FindAttitude(int attitudeId)
	    {
			var attitude = await _storage.Attitudes.FindAsync(attitudeId);
		    if (attitude == null)
			    throw new EntityNotFoundException(typeof(Attitude), attitudeId, "Find attitude method throws");

		    return attitude;
	    }

	    private async Task<int> SaveElevation(Elevation elevation)
	    {
		    var entry = await _storage.Elevations.AddAsync(elevation);
		    return entry.Entity.Id;
	    }

	    private async Task<int> SaveWorkflow(int attitudeNumber, long chatId)
	    {
		    var workflowStep = new Workflow
		    {
			    ChatId = chatId,
				CompletedAttitude = attitudeNumber
		    };
		    var historyEntry = await _storage.RateWorkflow.AddAsync(workflowStep);
		    return historyEntry.Entity.Id;
	    }

	    public async Task<Attitude> LoadNextAttitudeByChat(long chatId)
	    {
		    var workflowHistory = await _storage.RateWorkflow
			    .Where(r => r.ChatId.Equals(chatId))?.OrderByDescending(w => w.CompletedAttitude)
			    .FirstOrDefaultAsync();
		    if (workflowHistory == null)
				return await _storage.Attitudes.FirstOrDefaultAsync(a => a.Number.Equals(Attitude.FirstAttitudeNumber));

		    var nextAttitude = workflowHistory.CompletedAttitude++;
		    return await _storage.Attitudes.FirstOrDefaultAsync(a => a.Number.Equals(nextAttitude));
	    }

	    public async Task<List<Elevation>> LoadChatElevations(long chatId)
	    {
		    var elevationsList = await _storage.Elevations.Include(e => e.Attitude)
			    .Where(e => e.ChatId.Equals(chatId) && ElevationIsActual(e)).ToListAsync();
		    return elevationsList;
	    }

	    private bool ElevationIsActual(Elevation e)
	    {
		    return e.ElevationDate < DateTime.UtcNow && e.ElevationDate > ElevationLowerBoundary(e);
	    }

	    private DateTime ElevationLowerBoundary(Elevation e)
	    {
		    return DateTime.UtcNow - ElevationActualTime(e);
	    }

		private TimeSpan ElevationActualTime(Elevation e)
	    {
		    return e.NextElevationDate - e.ElevationDate;
	    }
		
	    public async Task<ElevationsHistory> LoadChatElevationsHistory(long chatId)
	    {
		    var chatElevations = await _storage.Elevations.Include(e => e.Attitude)
			    .Where(e => e.ChatId.Equals(chatId)).ToListAsync();
			var elevationsHistoryProvider = new ElevationsHistory(chatElevations, chatId);
		    return elevationsHistoryProvider;
	    }
    }
}
