﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MyWay.BL.BusinessEntities.Elevations;
using MyWayBot.Storage.Commands.Rate;

namespace MyWay.BL.BusinessEntities
{
    public class ElevationsHistory
    {
	    private ICollection<Elevation> _source;
	    public long ChatId { get; private set; }

	    public ElevationsHistory(ICollection<Elevation> source, long chatId)
	    {
		    _source = source;
		    ChatId = chatId;
	    }

	    public List<ElevationHistoryLine> History()
	    {
		    return _source.Select(e => new ElevationHistoryLine()
		    {
			    Attitude = e.Attitude.Text,
			    ElevationDate = e.ElevationDate,
			    Rate = e.Rate,
			    Comment = e.Comment
		    }).ToList();
	    }

		public List<HistoryDifference> AllTimeDifference()
	    {
			// max elevation date -> first elevation date
		    var orderedSource = _source.OrderByDescending(e => e.ElevationDate).ToArray();
		    var firstElevationDate = orderedSource.Last().ElevationDate;
		    var lastElevationDate = orderedSource.First().ElevationDate;
		    if (lastElevationDate.Equals(firstElevationDate))
			    return CalculateDifferences(orderedSource, orderedSource);

		    var firstElevations = _source.Where(e => e.ElevationDate.Equals(firstElevationDate)).ToArray();
		    var actualElevations = _source.Where(e => e.ElevationDate.Equals(lastElevationDate)).ToArray();
		    var historyDifferences = CalculateDifferences(firstElevations, actualElevations);
		    return historyDifferences;
	    }
		
	    private List<HistoryDifference> CalculateDifferences(Elevation[] firstElevations, Elevation[] lastElevations)
	    {
		    var actualElevations = OrderByAttitudeNumber(lastElevations);
		    var initialElevations = OrderByAttitudeNumber(firstElevations);
		    return MoveAndDiff(initialElevations, actualElevations);
	    }

	    private Elevation[] OrderByAttitudeNumber(Elevation[] source)
	    {
		    return source.OrderBy(e => e.Attitude.Number).ToArray();
	    }

	    private List<HistoryDifference> MoveAndDiff(Elevation[] initialSource, Elevation[] actualSource)
	    {
		    var minLength = initialSource.Length > actualSource.Length ? actualSource.Length : initialSource.Length;
			var differences = new List<HistoryDifference>(minLength);
		    for (var i = 0; i < minLength; i++)
		    {
			    differences.Add(DifferenceBetween(initialSource[i], actualSource[i]));
		    }
		    return differences;
	    }

	    private HistoryDifference DifferenceBetween(Elevation init, Elevation actual)
	    {
		    return new HistoryDifference()
		    {
			    AttitudeText = init.Attitude.Text,
			    FirstElevationDate = init.ElevationDate,
			    ActualElevationDate = actual.ElevationDate,
			    Difference = actual.Rate - init.Rate,
			    InitialComment = init.Comment,
			    ActualComment = actual.Comment
		    };
	    }

    }
}
