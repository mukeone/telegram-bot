﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWay.BL.BusinessEntities.Elevations
{
    public class HistoryDifference
    {
		public string AttitudeText { get; set; }
		public int Difference { get; set; }
		public DateTime FirstElevationDate { get; set; }
		public DateTime ActualElevationDate { get; set; }
		public string InitialComment { get; set; }
		public string ActualComment { get; set; }
    }
}
