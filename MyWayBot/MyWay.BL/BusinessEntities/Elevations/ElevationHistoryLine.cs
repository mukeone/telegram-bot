﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWay.BL.BusinessEntities.Elevations
{
    public class ElevationHistoryLine
    {
		public string Attitude { get; set; }
		public DateTime ElevationDate { get; set; }
		public int Rate { get; set; }
		public string Comment { get; set; }
    }
}
