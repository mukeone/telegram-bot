﻿using System;
using System.Collections.Generic;
using System.Text;
using MyWayBot.Storage.Commands.Rate;

namespace MyWay.BL.BusinessEntities
{
    public class ElevationBuilder
    {
	    private Elevation _elevation;
	    private int _monthesToNextElevation;

	    private ElevationBuilder(int monthesDiff)
	    {
		    _elevation = new Elevation();
		    _monthesToNextElevation = monthesDiff;
	    }

	    public static ElevationBuilder Builder(int monthesToNextElevation = 3)
	    {
		    return new ElevationBuilder(monthesToNextElevation);
	    }

	    public ElevationBuilder ChatId(long chatId)
	    {
		    _elevation.ChatId = chatId;
		    return this;
	    }

	    public ElevationBuilder Dependencies(int attitudeId)
	    {
		    _elevation.AttitudeId = attitudeId;
		    return this;
	    }

	    public ElevationBuilder Rate(int rate, string comment)
	    {
		    _elevation.Rate = rate;
		    _elevation.Comment = comment;
		    return this;
	    }

	    public Elevation Build()
	    {
		    _elevation.ElevationDate = DateTime.Today;
			_elevation.NextElevationDate = DateTime.Today.AddMonths(_monthesToNextElevation);
		    return _elevation;
	    }
    }
}
