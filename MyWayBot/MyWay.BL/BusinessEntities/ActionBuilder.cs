﻿using System;
using System.Collections.Generic;
using System.Text;
using MyWayBot.Storage.Solution;

namespace MyWay.BL.BusinessEntities
{
    public class ActionBuilder
    {
	    private CommandsController _actionRecord;

		private ActionBuilder()
	    {
		    _actionRecord = new CommandsController();
	    }

		public static ActionBuilder Builder => new ActionBuilder();

		public ActionBuilder SetChat(long chatId)
	    {
		    _actionRecord.ChatId = chatId;
		    return this;
	    }

	    public ActionBuilder Command(string command)
	    {
		    _actionRecord.Command = command;
		    return this;
	    }

	    public CommandsController Build()
	    {
		    _actionRecord.Date = DateTime.UtcNow;
		    return _actionRecord;
	    }
    }
}
