﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWay.BL.Exceptions
{
    public class EntityNotFoundException:Exception
    {
	    private Type _typeThrowed;
	    private int _primaryKey;

	    public EntityNotFoundException(Type typeThrowed, int primaryKey, string message):base(message)
	    {
		    _typeThrowed = typeThrowed;
		    _primaryKey = primaryKey;
	    }

	    public string Description => $"Entity {_typeThrowed.FullName} with primary key {_primaryKey} was not found";
    }
}
