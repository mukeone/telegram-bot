﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MyWay.BL.Bot.Commands
{
    public interface ICommand
    {
	    string Name { get; set; }
		Task Execute(Message message, ITelegramBotClient client);
    }
}
