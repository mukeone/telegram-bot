﻿using System.Collections.Generic;
using System.Linq;
using MyWay.BL.Bot.Commands.BotCommands;
using MyWay.BL.Bot.Commands.BotStates;
using MyWay.BL.Bot.Commands.BotStates.RateStates;

namespace MyWay.BL.Bot.Commands
{
    public class CommandsSource
    {
		internal IDialogState DialogState { get; set; }
		internal bool IsSuccessfulCommandExecution { get; set; }

		private IReadOnlyList<CommandInfo> _commands;

	    private void Initialize()
	    {
		    var startBotCommandInfo = new CommandInfo(new StartCommand(), "Start bot command");
			var startElevationCommandInfo = new CommandInfo(new StartElevationCommand(), "Estimate your skills");
			_commands = new List<CommandInfo>
			{
				startBotCommandInfo,
				startElevationCommandInfo
			};
	    }

	    public CommandsSource()
	    {
			Initialize();
	    }

	    public ICommand FindHandler(string name)
	    {
		    if (DialogState is WaitGlobalCommandState)
		    {
				var commandInfo = _commands.FirstOrDefault(i => i.Name.Contains(name));
			    if (commandInfo == null)
				    return new CommandNotFoundHandler();
				
				DialogState.Switch(this);
		    }
			

		    return commandInfo.Command;
	    }
    }
}
