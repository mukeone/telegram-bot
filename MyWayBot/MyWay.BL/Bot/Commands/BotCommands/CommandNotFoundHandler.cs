﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MyWay.BL.Bot.Commands.BotCommands
{
    public class CommandNotFoundHandler : ICommand
    {
	    public CommandNotFoundHandler()
	    {
		    Name = "SYSTEM_COMMAND";
	    }
	    public string Name { get; set; }
	    public async Task Execute(Message message, ITelegramBotClient client)
	    {
		    await client.SendTextMessageAsync(message.Chat.Id, "Command is not correct. Try again.");
	    }
    }
}
