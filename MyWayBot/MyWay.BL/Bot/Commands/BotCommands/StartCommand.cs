﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MyWay.BL.Bot.Commands.BotCommands
{
    public class StartCommand : ICommand
    {
	    public StartCommand()
	    {
		    Name = "/start";
		    _initialMessage = @"Вітаю, я допоможу тобі зрозуміти, 
							 як швидко та ефективно ти розвиваєшся. 
							Почни зараз та оціни свій прогрес.";
	    }

	    private string _initialMessage;

	    public string Name { get; set; }
	    public async Task Execute(Message message, ITelegramBotClient client)
	    {
		    var chat = message.Chat.Id;
		    await client.SendTextMessageAsync(chat, _initialMessage);
	    }
    }
}
