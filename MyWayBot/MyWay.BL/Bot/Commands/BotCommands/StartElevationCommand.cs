﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MyWay.BL.Bot.Commands.BotCommands
{
    public class StartElevationCommand : ICommand
    {
	    public StartElevationCommand()
	    {
		    Name = "/Elevation";
		    _initialMessage =
			    $"Ця команда допоже тобі визначити свій рівень підготовки та оцінити прогрес. " +
			    $"Використовуй формат " +
			    $"'оцінка від 1 до 10. Короткий опис свого розвитку в запропонованій сфері'";
	    }

	    public string Name { get; set; }

	    private string _initialMessage;

	    public async Task Execute(Message message, ITelegramBotClient client)
	    {
			var chat = message.Chat.Id;
		    await client.SendTextMessageAsync(chat, _initialMessage);
		}
    }
}
