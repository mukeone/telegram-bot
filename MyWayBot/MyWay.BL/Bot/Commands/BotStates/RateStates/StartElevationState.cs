﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWay.BL.Bot.Commands.BotStates.RateStates
{
    public class StartElevationState : IDialogState
    {
	    private int _attitudesCount;
	    public StartElevationState(int attitudesCount)
	    {
		    _attitudesCount = attitudesCount;
	    }

	    public void Switch(CommandsSource context)
	    {
		    context.DialogState = new WaitAnswerState(_attitudesCount);
	    }
    }
}
