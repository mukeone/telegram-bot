﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWay.BL.Bot.Commands.BotStates.RateStates
{
    public class WaitAnswerState : IDialogState
    {
	    private int _waitAnswers;

	    public WaitAnswerState(int answersCount)
	    {
		    _waitAnswers = answersCount;
	    }

	    public void Switch(CommandsSource context)
	    {
		    if (context.IsSuccessfulCommandExecution)
		    {
			    _waitAnswers--;
		    }
		    if (_waitAnswers > 0) return;
			// Return to the commands menu
			context.DialogState = new WaitGlobalCommandState();
	    }
    }
}
