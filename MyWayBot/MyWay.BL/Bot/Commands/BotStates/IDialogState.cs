﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyWay.BL.Bot.Commands.BotStates
{
    public interface IDialogState
    {
	    void Switch(CommandsSource context);
    }
}
