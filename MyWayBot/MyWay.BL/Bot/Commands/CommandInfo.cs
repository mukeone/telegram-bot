﻿namespace MyWay.BL.Bot.Commands
{
    public class CommandInfo
    {
		public string Description { get; set; }
		public string Name { get; set; }
		public ICommand Command { get; set; }

	    public CommandInfo(ICommand command, string description)
	    {
		    Command = command;
		    Name = command.Name;
		    Description = description;
	    }
    }
}
