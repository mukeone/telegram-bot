﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyWayBot.API.Handlers.Commands;

namespace MyWayBot.API.Handlers
{
    public class CommandInfo
    {
		public string Description { get; set; }
		public string Name { get; set; }
		public ICommand Command { get; set; }

	    public CommandInfo(ICommand command, string description)
	    {
		    Command = command;
		    Name = command.Name;
		    Description = description;
	    }
    }
}
