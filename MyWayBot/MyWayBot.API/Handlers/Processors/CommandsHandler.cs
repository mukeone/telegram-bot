﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using MyWayBot.API.Core;

namespace MyWayBot.API.Handlers.Processors
{
    public class CommandsHandler : ICommandProcessor
    {
	    private CommandsSource commands;
	    private ITelegramBotClient _bot;

	    public CommandsHandler()
	    {
		    commands = new CommandsSource();
		    _bot = InitializeBot();

	    }

	    private ITelegramBotClient InitializeBot()
	    {
		    var bot = new TelegramBotClient(Configuration.ApiKey);
		    bot.SetWebhookAsync(Configuration.Url);
		    return bot;
	    }

	    public async Task Handle(Message message)
	    {
		    var command = commands.FindHandler(message.Text);
		    await command.Execute(message, _bot);
	    }
    }
}
