﻿using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace MyWayBot.API.Handlers.Processors
{
    public interface ICommandProcessor
    {
	    Task Handle(Message message);
    }
}
