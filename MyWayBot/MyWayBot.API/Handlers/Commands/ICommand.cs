﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace MyWayBot.API.Handlers.Commands
{
    public interface ICommand
    {
	    string Name { get; set; }
		Task Execute(Message message, ITelegramBotClient client);
    }
}
