﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyWayBot.API.Handlers.Commands;
using MyWayBot.API.Handlers.Commands.BotCommands;

namespace MyWayBot.API.Handlers
{
    public class CommandsSource
    {
	    private IReadOnlyList<CommandInfo> _commands;

	    public CommandsSource()
	    {
		    _commands = new List<CommandInfo>()
		    {
			    new CommandInfo(new StartCommand(), "Start bot command")
				// Add new commands
		    };
	    }

	    public ICommand FindHandler(string name)
	    {
		    var commandInfo = _commands.FirstOrDefault(i => i.Name.Contains(name));
			if(commandInfo == null)
				return new CommandNotFoundHandler();

		    return commandInfo.Command;
	    }
    }
}
