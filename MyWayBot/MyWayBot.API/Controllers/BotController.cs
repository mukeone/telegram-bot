﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyWayBot.API.Handlers.Processors;
using Telegram.Bot.Types;

namespace MyWayBot.API.Controllers
{
    [Route("api/[controller]")]
    public class BotController : Controller
    {
	    private ICommandProcessor processor;

	    public BotController(ICommandProcessor processor)
	    {
		    this.processor = processor;
	    }
		
	    [HttpPost]
	    public async Task<OkResult> Post([FromBody] Update update)
	    {
		    await processor.Handle(update.Message);
		    return Ok();
	    }
    }
}
